<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('objects_search')) {
    function objects_search($array, $searched_property, $searched_value) {

        foreach ($array as $item) {
            if($item->{$searched_property} == $searched_value) {
                return $item;
            }
        }

        return NULL;
    }
}
