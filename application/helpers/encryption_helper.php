<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('encrypt')) {
    function encrypt($value) {
        $CI =& get_instance();
        $CI->load->library('encryption');
        return $CI->encryption->encrypt($value);
    }
}

if (!function_exists('decrypt')) {
    function decrypt(string $value) {
        $CI =& get_instance();
        $CI->load->library('encryption');
        return $CI->encryption->decrypt($value);
    }
}

if (!function_exists('custom_hash')) {
    function custom_hash(string $value) {
        $CI =& get_instance();
        $value = $CI->config->item('encryption_key').$value;
        return crypt($value, 'sha-256');
    }
}
