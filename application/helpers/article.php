<!-- Post -->
<article class="post">
    <header>
        <div class="title">
            <h2><a href="#">Ultricies sed magna euismod enim vitae gravida</a></h2>
            <p>Lorem ipsum dolor amet nullam consequat etiam feugiat</p>
        </div>
        <div class="meta">
            <time class="published" datetime="2015-10-25">October 25, 2015</time>
            <a href="#" class="author"><span class="name">Jane Doe</span><img src="<?= img_url('avatar.jpg') ?>" alt="" /></a>
        </div>
    </header>
    <a href="#" class="image featured"><img src="<?= img_url('pic02.jpg') ?>" alt="" /></a>
    <p>Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper.</p>
    <footer>
        <ul class="stats">
            <li><a href="#">General</a></li>
            <li><a href="#" class="icon fa-heart">28</a></li>
            <li><a href="#" class="icon fa-comment">128</a></li>
        </ul>
    </footer>
</article>
