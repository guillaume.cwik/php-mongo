<?php

/**
 *  Objet User : représente un utilisateur
 */
class User extends Object {

    public $login = null;
    public $password = null;
    public $pseudo = null;
    public $img_url = null;
    public $likes = [];
    public $created_at = null;
    public $updated_at = null;

    public function __construct(string $login, string $pseudo, string $password, string $img_url = '') {
        parent::__construct();
        $this->login = $login;
        $this->pseudo = $pseudo;
        $this->password = custom_hash($password);
        $this->img_url = $img_url;

        $this->created_at = new MongoDB\BSON\UTCDateTime;
    }

    // public function get_articles() {
    //
    // }
    //
    // public function get_liked_articles() {
    //
    // }
}
