<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('assets_url')) {
    function assets_url($file)
    {
        return base_url('assets/'.$file);
    }
}

if (!function_exists('js_url')) {
    function js_url($file)
    {
        return assets_url('js/'.$file);
    }
}

if (!function_exists('css_url')) {
    function css_url($file)
    {
        return assets_url('css/'.$file);
    }
}

if (!function_exists('img_url')) {
    function img_url($file)
    {
        return assets_url('img/'.$file);
    }
}

if(!function_exists('current_url')) {
    function current_url($extra_get_params = []) {
        $get_params = array_merge($_GET, $extra_get_params);
        $url_query = http_build_query($get_params);
        return site_url(uri_string().'?'.$url_query);
    }
}
