<?php

/**
 * Classe récupérant les données concernants les users en BDD
 */
class Article_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->collection =& $this->db->article;
    }
}
