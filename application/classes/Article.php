<?php

/**
 *  Objet Article : représente un article
 */
class Article extends Object {

    public $title = null;
    public $subtitle = null;
    public $content = null;
    public $author = null;
    public $img_url = null;
    public $tags = [];
    public $likes = [];
    public $comments = [];
    public $created_at = null;
    public $updated_at = null;

    public function __construct(
        string $title,
        string $subtitle,
        string $content,
        User $author,
        string $img_url = '',
        array $tags = []
    ) {
        parent::__construct();
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->content = $content;
        $this->author = $author->_id;
        $this->img_url = $img_url;
        $this->tags = $tags;
        $this->created_at = new MongoDB\BSON\UTCDateTime;
    }

    public function assign_author() {
        $this->author = User::find_by_id($this->author);
    }

    public static function assign_authors(array &$articles) {
        $authors_ids = array_column($articles, "author");

        $authors = User::find_many(['_id' => ['$in' => $authors_ids]]);

        foreach ($articles as $article) {
            $article->author = objects_search($authors, '_id', $article->author);
        }
    }

    // public function get_articles() {
    //
    // }
    //
    // public function get_liked_articles() {
    //
    // }
}
