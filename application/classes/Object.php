<?php

// TODO : pour les fonctions de CRUD, autoriser les objets en plus des array

/**
 * Permet d'utiliser le type Object pour toutes les classes du répertoires APPPATH/classes
 */
class Object implements MongoDB\BSON\Persistable {

    public $_id = null;

    public function __construct() { }

    // Nom de la fonction par défaut, ne peux pas être mis au format 'snake_case' (format préféré par CI)
    public function bsonSerialize() : array {
        return (array) $this;
    }

    // Nom de la fonction par défaut, ne peux pas être mis au format 'snake_case' (format préféré par CI)
    public function bsonUnserialize(array $data) : Object {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }

        return $this;
    }

    protected static function &_load_model() : CI_Model {
        $model_name = strtolower(static::class).'_model';
        $CI =& get_instance();
        $CI->load->model($model_name);
        return $CI->{$model_name};
    }

    public function save(array $options = []) {
        if(empty($this->_id)) {
            unset($this->_id);
        }

        $model = static::_load_model();
        return $model->insert_one($this, $options);
    }

    public static function save_many(array $objects = [], array $options = []) {
        foreach ($objects as $object) {
            if(empty($object->_id)) {
                unset($object->_id);
            }
        }

        $model = static::_load_model();
        return $model->insert_many($objects, $options);
    }

    public function update(array $options = []) {
        if(empty($this->_id)) {
            throw new Exception("No ID for this document", 1);
        }

        $model = static::_load_model();
        return $model->replace_one(['_id' => $this->_id], $this, $options);
    }

    public static function update_one(array $filter = [], $update = [], array $options = []) {
        $model = static::_load_model();
        return $model->update_one($filter, $update);
    }

    public static function update_many(array $filter = [], $update = [], array $options = []) {
        $model = static::_load_model();
        return $model->update_many($filter, $update);
    }

    public function delete(array $options = []) {
        if(empty($this->_id)) {
            throw new Exception("No ID for this document", 1);
        }

        $model = static::_load_model();
        return $model->delete_one(['_id' => $this->_id], $options);
    }

    public static function delete_many(array $filter = [], array $options = []) {
        $model = static::_load_model();
        return $model->delete_many($filter, $options);
    }

    public static function find_by_id($id, array $options = []) : ?Object {
        $model = static::_load_model();

        if(is_string($id)) {
            $id = new MongoDB\BSON\ObjectID($id);
        }
        
        return $model->find_one(['_id' => $id], $options);
    }

    public static function find_one(array $filter = [], array $options = []) : ?Object {
        $model = static::_load_model();
        return $model->find_one($filter, $options);
    }

    public static function find_many(array $filter = [], array $options = []) : array {
        $model = static::_load_model();

        return $model->find($filter, $options)->toArray();
    }

    public static function count(array $filter = [], array $options = []) {
        $model = static::_load_model();
        return $model->count($filter, $options);
    }
}
