<?php

/**
 *  Objet Comment : représente un commentaire
 */
class Comment extends Object {

    public $author = null;
    public $message = null;
    public $created_at = null;

    public function __construct(string $message, User $author) {
        parent::__construct();
        $this->message = $message;
        $this->author = $author->_id;

        $this->created_at = new MongoDB\BSON\UTCDateTime;
    }

    public function assign_author() {
        $this->author = User::find_by_id($this->author);
    }

    public static function assign_authors(ArrayObject &$comments) {
        $authors_ids = array_column((array) $comments, "author");

        $authors = User::find_many(['_id' => ['$in' => $authors_ids]]);

        foreach ($comments as $comment) {
            $comment->author = objects_search($authors, '_id', $comment->author);
        }
    }
}
