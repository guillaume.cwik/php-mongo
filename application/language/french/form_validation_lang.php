<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= 'Le champ "{field}" est requis.';
$lang['form_validation_isset']			= 'Le champ "{field}" doit être renseigné.';
$lang['form_validation_valid_email']		= 'Le champ "{field}" doit contenir une adresse email valide.';
$lang['form_validation_valid_emails']		= 'Le champ "{field}" doit contenir des adresses emails valides.';
$lang['form_validation_valid_url']		= 'Le champ "{field}" doit contenir une URL valide.';
$lang['form_validation_valid_ip']		= 'Le champ "{field}" doit contenir une adresse IP valide.';
$lang['form_validation_min_length']		= 'Le champ "{field}" doit faire une longueur de {param} caractères.';
$lang['form_validation_max_length']		= 'Le champ "{field}" ne doit pas dépasser {param} caractères.';
$lang['form_validation_exact_length']		= 'Le champ "{field}" doit faire exactement {param} caractères.';
$lang['form_validation_alpha']			= 'Le champ "{field}" doit contenir uniquement des caractères alphabétique.';
$lang['form_validation_alpha_numeric']		= 'Le champ "{field}" doit contenir uniquement des caractères alphanuméric.';
$lang['form_validation_alpha_numeric_spaces']	= 'Le champ "{field}" doit contenir uniquement des caractères alphanuméric et des espaces.';
$lang['form_validation_alpha_dash']		= 'Le champ "{field}" doit contenir uniquement des caractères alphanuméric, des underscores et des tirets.';
$lang['form_validation_numeric']		= 'Le champ "{field}" doit contenir uniquement des nombres.';
$lang['form_validation_is_numeric']		= 'Le champ "{field}" doit contenir uniquement des caractères numérique.';
$lang['form_validation_integer']		= 'Le champ "{field}" doit contenir un chiffre entier.';
$lang['form_validation_regex_match']		= 'Le champ "{field}" n\'a pas le bon format.';
$lang['form_validation_matches']		= 'Le champ "{field}" ne correspond pas au champ "{param}".';
$lang['form_validation_differs']		= 'Le champ "{field}" doit être différent du champ "{param}".';
$lang['form_validation_is_unique'] 		= 'La valeur renseigné dans le champ "{field}" existe déjà.';
$lang['form_validation_is_natural']		= 'Le champ "{field}" doit must only contain digits.';
$lang['form_validation_is_natural_no_zero']	= 'Le champ "{field}" doit must only contain digits and must be greater than zero.';
$lang['form_validation_decimal']		= 'Le champ "{field}" doit must contain a decimal number.';
$lang['form_validation_less_than']		= 'Le champ "{field}" doit must contain a number less than {param}.';
$lang['form_validation_less_than_equal_to']	= 'Le champ "{field}" doit must contain a number less than or equal to {param}.';
$lang['form_validation_greater_than']		= 'Le champ "{field}" doit must contain a number greater than {param}.';
$lang['form_validation_greater_than_equal_to']	= 'Le champ "{field}" doit must contain a number greater than or equal to {param}.';
$lang['form_validation_error_message_not_set']	= 'Unable to access an error message corresponding to your field name "{field}".';
$lang['form_validation_in_list']		= 'Le champ "{field}" doit must be one of: {param}.';
