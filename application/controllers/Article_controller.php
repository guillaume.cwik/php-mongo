<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller permettant l'affichage de tous les articles (une catégorie peut être choisi)
 */
class Article_controller extends MY_Controller {



	public function show($article_id)	{
		$this->data['article'] = Article::find_by_id($article_id);
		$this->data['article']->assign_author();
		Comment::assign_authors($this->data['article']->comments);

        $this->_load_header();
		$this->load->view('article', $this->data);
        $this->_load_footer();
	}

	public function add() {
		if(empty($this->connected_user)) {
			redirect('/');
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Titre', 'required|min_length[3]|max_length[150]');
		$this->form_validation->set_rules('subtitle', 'Sous-titre', 'min_length[1]|max_length[50]');
		$this->form_validation->set_rules('tags', 'Tags', 'regex_match[/^[a-zA-Z0-9;# ]+$/]');
		$this->form_validation->set_rules('content', 'Confirmation mot de passe', 'required');
		$this->form_validation->set_rules('img-url', 'Image de l\'article', 'valid_url');

		if($this->form_validation->run() === FALSE) {
			$this->_load_header();
			$this->load->view('article-add');
			$this->_load_footer();
		} else {
			$article = new Article(
				$this->input->post('title'),
				$this->input->post('subtitle'),
				$this->input->post('content'),
				$this->connected_user,
				$this->input->post('img-url'),
				array_map('trim', explode(';', $this->input->post('tags')))
			);

			$article->save();
			redirect('/');
		}
	}

	public function comment() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('article', 'Article', 'required');
		$this->form_validation->set_rules('comment', 'Commentaire', 'required|min_length[2]');

		if($this->form_validation->run() && $this->connected_user) {
			$comment = new Comment($this->input->post('comment'), $this->connected_user);

			$article = Article::find_by_id($this->input->post('article'));

			if($article) {
				$article->comments[] = $comment;
				$article->update();
			}

			redirect('/article/show/'.$article->_id);
		}

		redirect('/');
	}

	public function delete($article_id) {
		$article = Article::find_by_id($article_id);
		$user = $this->connected_user;

		if($article->author == $user->_id) {
			$article->delete();
		}

		redirect('/');
	}

	public function like($article_id) {
		$article = Article::find_by_id($article_id);
		if(!in_array($this->connected_user->_id, (array) $article->likes)) {
			$article->likes[] = $this->connected_user->_id;
			$article->update();
		}

		redirect('/article/show/'.$article->_id);
	}

	public function dislike($article_id) {
		$article = Article::find_by_id($article_id);
		if(in_array($this->connected_user->_id, (array) $article->likes)) {
			$article->likes = array_diff($article->likes, [$this->connected_user->_id]);
			$article->update();
		}

		redirect('/article/show/'.$article->_id);
	}
}
