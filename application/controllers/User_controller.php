<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller permettant l'affichage de tous les articles (une catégorie peut être choisi)
 */
class User_controller extends MY_Controller {

	public function index() {
		// var_dump(User::find_one());

		// $test = new User('xyz', 'abc', custom_hash('abc'));
		// var_dump($test->save());
		// echo '<br  /><br  />';
		// echo '<br  /><br  />';
		// $test = User::find_one(['login' => 'xyz']);
		// $test->login = 'olol';
		// var_dump($test->update());
		// echo '<br  /><br  />';
		// echo '<br  /><br  />';
		// $test  = $test = User::find_one(['login' => 'olol']);
		// var_dump($test->delete());

		// $test = [
		// 	new User('xyz', 'abc', 'abc'),
		// 	new User('xyz', 'abc', 'abc'),
		// 	new User('xyz', 'abc', 'abc')
		// ];
		// var_dump(User::save_many($test));
		// echo '<br  /><br  />';
		// echo '<br  /><br  />';
		// var_dump(User::update_many(['login' => 'xyz'], ['$set' => ['login' => 'olol']]));
		// echo '<br  /><br  />';
		// echo '<br  /><br  />';
		// var_dump(User::delete_many(['login' => 'olol']));


		// var_dump(User::delete_many());

		// var_dump($this->user_model->findOne()['_id']);
        // $this->_load_header();
		// $this->load->view('inscription');
        // $this->_load_footer();
	}

	public function sign_up() {
		if($this->connected_user) {
			redirect('/blog/');
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('login', 'Nom d\'utilisateur', 'required|min_length[5]|max_length[30]|alpha_dash|callback_is_unique[user.login]');
		$this->form_validation->set_rules('pseudo', 'Pseudo', 'required|min_length[5]|max_length[30]|alpha_dash|callback_is_unique[user.pseudo]');
		$this->form_validation->set_rules('password', 'Mot de passe', 'required|min_length[8]|max_length[50]');
		$this->form_validation->set_rules('password-confirm', 'Confirmation mot de passe', 'required|matches[password]');
		$this->form_validation->set_rules('img-url', 'Url de l\'image de profil', 'valid_url');

		if($this->form_validation->run() === FALSE) {
			$this->_load_header();
			$this->load->view('sign_up');
			$this->_load_footer();
		} else {

			$user = new User(
				$this->input->post('login'),
				$this->input->post('pseudo'),
				$this->input->post('password'),
				$this->input->post('img-url')
			);

			$user->save();
			redirect('/user/sign_in');
		}
	}

	public function sign_in() {

		if($this->input->post('login') == TRUE) {
			$user = User::find_one([
				'login' => $this->input->post('login'),
				'password' => custom_hash($this->input->post('password'))
			]);

			if(!empty($user)) {
				$this->session->set_userdata('user', $user);
			} else {
				$this->load->library('form_validation');
				$this->form_validation->set_error('login', 'Le nom d\'utilisateur ou le mot de passe est incorrect');
			}
		}

		if($this->connected_user) {
			redirect('/');
		}

		$this->_load_header();
		$this->load->view('sign_in');
		$this->_load_footer();
	}

	public function sign_out() {
		$this->session->sess_destroy();
		redirect('/');
	}


}
