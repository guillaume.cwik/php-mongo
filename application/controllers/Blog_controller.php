<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller permettant l'affichage de tous les articles (une catégorie peut être choisi)
 */
class Blog_controller extends MY_Controller {


	public function index() {
		// Ajouter des sorts de filtres (articles de l'auteur X, articles avec le tags X)
		$filter = [];
		if($author_id = $this->input->get('author')) {
			$filter['author'] = new MongoDB\BSON\ObjectID($author_id);
		}

		if($tag = $this->input->get('tag')) {
			$filter['tags'] = new MongoDB\BSON\Regex("^$tag$", 'i');
		}

		$options = [];
		$options['limit'] = 3;
		$options['sort'] = ['created_at' => -1];

		if($num_page = $this->input->get('page')) {
			$options['skip'] = ($num_page - 1) * 3;
		} else {
			$num_page = 1;
		}

		$this->data['articles'] = Article::find_many($filter, $options);

		$options['sort'] = ['likes' => -1, 'title' => 1];
		$this->data['liked_articles'] = Article::find_many($filter, $options);

		$options['sort'] = ['comments' => -1, 'title' => 1];
		$this->data['commented_articles'] = Article::find_many($filter, $options);
		Article::assign_authors($this->data['articles']);

		$this->data['nb_articles'] = Article::count($filter);
		$this->data['num_page'] = $num_page;

        $this->_load_header();
		$this->load->view('blog', $this->data);
        $this->_load_footer();
	}
}
