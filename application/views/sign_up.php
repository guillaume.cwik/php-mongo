<div class="container">
    <h3>Inscription</h3>
    <form class="form_inscription" method="post">
        <label for="login">Nom d'utilisateur</label>
        <?= form_error('login', '<div class="error">', '</div>') ?>
        <input type="text" name="login" value="<?= set_value('login') ?>" placeholder="login">
        <br />
        <label for="pseudo">Pseudo</label>
        <?= form_error('pseudo', '<div class="error">', '</div>') ?>
        <input type="text" name="pseudo" value="<?= set_value('pseudo') ?>" placeholder="pseudo">
        <br />
        <label for="img-url">URL de l'image de profil</label>
        <?= form_error('img-url', '<div class="error">', '</div>') ?>
        <input type="text" name="img-url" value="<?= set_value('img-url') ?>" placeholder="http://.../image.jpg">
        <br />
        <label for="password">Mot de passe</label>
        <?= form_error('password', '<div class="error">', '</div>') ?>
        <input type="password" name="password" placeholder="password">
        <label for="password-confirm">Mot de passe - confirmation</label>
        <?= form_error('password-confirm', '<div class="error">', '</div>') ?>
        <input type="password" name="password-confirm" placeholder="password" onPaste="return false">
        <br  /><br  />
        <button href="#" class="button big fit">Inscription</a>
    </form>
</div>
