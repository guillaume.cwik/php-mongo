<div id="main">
    <article class="post fullpage">
        <header>
            <div class="title">
                <h2>
                    <a href="<?= site_url('/article/show/'.$article->_id) ?>"><?= $article->title ?></a>
                    <a href="<?= site_url('/article/delete/'.$article->_id) ?>"><i class="fa fa-times delete" aria-hidden="true"></i></a>
                </h2>
                <p><?= $article->subtitle ?></p>
            </div>
            <div class="meta">
                <?php $article_datetime = $article->created_at->toDateTime(); ?>
                <time class="published" datetime="<?= $article_datetime->format('Y-m-d') ?>"><?= $article_datetime->format('d/m/Y') ?></time>
                <a href="<?= site_url('/blog/') ?>?author=<?= $article->author->_id ?>" class="author">
                    <span class="name"><?= $article->author->pseudo ?></span>
                    <?php if(!empty($article->author->img_url)) : ?>
                        <img src="<?= $article->author->img_url ?>" alt="" />
                    <?php endif; ?>
                </a>
            </div>
        </header>
        <?php if(!empty($article->img_url)) : ?>
            <a href="<?= site_url('/article/show/'.$article->_id) ?>" class="image featured"><img src="<?= $article->img_url ?>" alt="" /></a>
        <?php endif; ?>
        <p><?=	$article->content ?></p>
        <footer>
            <ul class="stats">
                <?php for($i = 0; $i < count($article->tags); $i++) : ?>
                    <li><a href="<?= site_url('/blog/') ?>?tag=<?= $article->tags[$i] ?>"><?= $article->tags[$i] ?></a></li>
                <?php endfor; ?>
                <li>
                    <?= count($article->likes) ?>
                    <?php if ($connected_user): ?>
                        <?php $already_liked = in_array($connected_user->_id, (array) $article->likes); ?>
                        <?php if ($already_liked): ?>
                            <a href="<?= site_url('article/dislike/'.$article->_id) ?>"><i class="fa fa-thumbs-up dislike" aria-hidden="true"></i></a>
                        <?php else: ?>
                            <a href="<?= site_url('article/like/'.$article->_id) ?>"><i class="fa fa-thumbs-up like" aria-hidden="true"></i></a>
                        <?php endif; ?>
                    <?php else : ?>
                            <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                    <?php endif; ?>
                </li>
                <li><?= count($article->comments) ?> <i class="fa fa-comment" aria-hidden="true"></i></li>
            </ul>
        </footer>
    </article>
<div />

<article class="fullpage">
    <h3>Commentaires</h3>
    <hr />
    <div class="comments">
        <?php foreach ($article->comments as $comment): ?>
            <div class="comment">
                <?= $comment->message ?>
                <div class="author">
                    <?php $comment_datetime = $comment->created_at->toDateTime(); ?>
                    <div class="name">
                        <?= $comment->author->pseudo ?><br />
                        <?= $comment_datetime->format('d/m/Y') ?><br />
                        <?= $comment_datetime->format('H\hi') ?>
                    </div>
                    <br />
                    <?php if (!empty($comment->author->img_url)) : ?>
                        <img src="<?= $comment->author->img_url ?>" alt="">
                    <?php endif; ?>
                </div>
            </div>
            <hr />
        <?php endforeach; ?>
        <?php if (!empty($connected_user)): ?>
            <div >
                <h6>Nouveau commentaire</h6>

                <form action="<?= site_url('article/comment/') ?>" method="post">
                    <input type="hidden" name="article" value="<?= $article->_id ?>">
                    <textarea name="comment" rows="4" cols="80"></textarea>
                    <br />
                    <input type="submit" class="button big" value="Commenter">
                    <div class="clear-fix"></div>
                </form>
            </div>
        <?php endif; ?>
    </div>
</article>
