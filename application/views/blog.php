
<!-- Main -->
<div id="main">
	<?php foreach($articles ?? [] as $article) : ?>
		<?php //var_dump($article); ?>
		<article class="post">
			<header>
				<div class="title">
					<h2>
						<a href="<?= site_url('/article/show/'.$article->_id) ?>"><?= $article->title ?></a>
						<?php if($connected_user && $article->author->_id == $connected_user->_id) : ?>
							<a href="<?= site_url('/article/delete/'.$article->_id) ?>"><i class="fa fa-times" aria-hidden="true" style="color: #b70315; float: right;"></i></a>
						<?php endif; ?>
					</h2>
					<p><?= $article->subtitle ?></p>
				</div>
				<div class="meta">
					<?php $article_datetime = $article->created_at->toDateTime(); ?>
					<time class="published" datetime="<?= $article_datetime->format('Y-m-d') ?>"><?= $article_datetime->format('d/m/Y') ?></time>
					<a href="<?= site_url('/blog/') ?>?author=<?= $article->author->_id ?>" class="author">
						<span class="name"><?= $article->author->pseudo ?></span>
						<?php if(!empty($article->author->img_url)) : ?>
							<img src="<?= $article->author->img_url ?>" alt="" />
						<?php endif; ?>
					</a>
				</div>
			</header>
			<?php if(!empty($article->img_url)) : ?>
				<a href="<?= site_url('/article/show/'.$article->_id) ?>" class="image featured"><img src="<?= $article->img_url ?>" alt="" /></a>
			<?php endif; ?>
			<p><?=	substr($article->content,0 ,150)  ?><?= (strlen($article->content) > 150 ? '...' : '') ?></p>
			<footer>
				<ul class="actions">
					<li><a href="<?= site_url('/article/show/'.$article->_id) ?>" class="button big">Continue Reading</a></li>
				</ul>
				<ul class="stats">
					<?php for($i = 0; $i < count($article->tags); $i++) : ?>
						<li><a href="<?= site_url('/blog/') ?>?tag=<?= $article->tags[$i] ?>"><?= $article->tags[$i] ?></a></li>
					<?php endfor; ?>
					<li><?= count($article->likes) ?> <i class="fa fa-thumbs-up" aria-hidden="true"></i></li>
					<li><?= count($article->comments) ?> <i class="fa fa-comment" aria-hidden="true"></i></li>
				</ul>
			</footer>
		</article>
	<?php endforeach; ?>

	<!-- Pagination -->
	<ul class="actions pagination">
		<?php $is_first_page = ($num_page <= 1); ?>
		<li><a href="<?= ($is_first_page ? '#' : current_url(['page' => $num_page-1])) ?>" class="button big previous <?= ($is_first_page ? 'disabled' : '') ?>">Previous Page</a></li>
		<?php $have_next_page = $nb_articles > ($num_page * 3); ?>
		<li><a href="<?= ($have_next_page ? current_url(['page' => $num_page+1]) : '#') ?>" class="button big next <?= ($have_next_page ? '' : 'disabled') ?>">Next Page</a></li>
	</ul>

</div>

<!-- Sidebar -->
<section id="sidebar">

	<!-- Intro -->
	<section id="intro">
		<a href="#" class="logo"><img src="<?= img_url('logo.jpg') ?>" alt="" /></a>
		<header>
			<h2>BlogIT</h2>
			<p>Un blog sur l'informatique et ses différents domaines</a></p>
		</header>
	</section>

	<!-- Mini Posts -->
	<section>
		<h4>Les plus aimés :</h4>
		<br />
		<div class="mini-posts">
			<?php foreach ($liked_articles as $article): ?>
				<article class="mini-post">
					<header>
						<h3><a href="<?= site_url('/article/show/'.$article->_id) ?>"><?= $article->title ?></a></h3>
						<?php $article_datetime = $article->created_at->toDateTime(); ?>
						<time class="published" datetime="<?= $article_datetime->format('Y-m-d') ?>"><?= $article_datetime->format('d/m/Y') ?></time>
						<?php if (!empty($article->author->img_url)): ?>
							<a href="<?= site_url('/blog/') ?>?author=<?= $article->author->_id ?>" class="author"><img src="<?= $article->author->img_url ?>" alt="" /></a>
						<?php endif; ?>
					</header>
					<?php if (!empty($article->img_url)): ?>
						<a href="<?= site_url('/article/show/'.$article->_id) ?>" class="image"><img src="<?= $article->img_url ?>" alt="" /></a>
					<?php endif; ?>
				</article>
			<?php endforeach; ?>
		</div>
	</section>

	<!-- Posts List -->
	<section>
		<h4>Les plus commentés :</h4>
		<br />
		<ul class="posts">
			<?php foreach ($commented_articles as $article): ?>
				<li>
					<article>
						<header>
							<h3><a href="<?= site_url('/article/show/'.$article->_id) ?>"><?= $article->title ?></a></h3>
							<?php $article_datetime = $article->created_at->toDateTime(); ?>
							<time class="published" datetime="<?= $article_datetime->format('Y-m-d') ?>"><?= $article_datetime->format('d/m/Y') ?></time>
						</header>
					</article>
				</li>
			<?php endforeach; ?>
		</ul>
	</section>

	<!-- About -->
	<section class="blurb">
		<h2>About</h2>
		<p>BLOGIT est un blog d'entreprise permettant aux collaborateurs de poster des articles de veille technologique afin de les partager et de les commenter</p>
	</section>
	<!-- Footer -->
	<!-- <section id="footer">
	<ul class="icons">
	<li><a href="#" class="fa-twitter"><span class="label">Twitter</span></a></li>
	<li><a href="#" class="fa-facebook"><span class="label">Facebook</span></a></li>
	<li><a href="#" class="fa-instagram"><span class="label">Instagram</span></a></li>
	<li><a href="#" class="fa-rss"><span class="label">RSS</span></a></li>
	<li><a href="#" class="fa-envelope"><span class="label">Email</span></a></li>
</ul>
<p class="copyright">&copy; Untitled. Crafted: <a href="http://designscrazed.org/">HTML5</a>.</p>
</section> -->

</section>

<?php $this->load->view('common/footer'); ?>
