<div class="container">
    <h3>Connexion</h3>
    <form class="form_connexion" method="post">
        <label for="login">Nom d'utilisateur</label>
        <?= form_error('login', '<div class="error">', '</div>') ?>
        <input type="text" name="login" placeholder="login">
        <br />
        <label for="password">Mot de passe</label>
        <input type="password" name="password" placeholder="password">
        <br  /><br  />
        <button href="#" class="button big fit">Connexion</a>
    </form>
</div>
