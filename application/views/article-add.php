<div class="container">
    <h3>Nouvel article</h3>
    <form class="form_connexion" method="post">
        <table class="no-background"> <!-- Changer le tableau en div mais pas le temps -->
            <tr>
                <td style="width: 50%;">
                    <label for="title">Titre *</label>
                    <?= form_error('title', '<div class="error">', '</div>') ?>
                    <input type="text" name="title" value="<?= set_value('title') ?>" placeholder="title">
                </td>
                <td>
                    <label for="subtitle">Sous-titre *</label>
                    <?= form_error('subtitle', '<div class="error">', '</div>') ?>
                    <input type="text" name="subtitle" value="<?= set_value('subtitle') ?>" placeholder="subtitle">
                </td>
            </tr>
            <tr>
                <td>
                    <label for="tags">Tags<i>- séparer par des points virgules</i></label>
                    <?= form_error('tags', '<div class="error">', '</div>') ?>
                    <input type="text" name="tags" value="<?= set_value('tags') ?>" placeholder="NoSQL; JavaScript; ...">
                </td>
                <td>
                    <label for="img-url">Image de l'article</label>
                    <?= form_error('img-url', '<div class="error">', '</div>') ?>
                    <input type="text" name="img-url" value="<?= set_value('img-url') ?>" placeholder="NoSQL; JavaScript; ...">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <label for="content">Contenu *</label>
                    <?= form_error('content', '<div class="error">', '</div>') ?>
                    <textarea name="content" rows="8" cols="80"><?= set_value('content') ?></textarea>
                </td>
            </tr>
        </table>

        <!-- <label for="login">Nom d'utilisateur</label>
        <?= form_error('img-url', '<div class="error">', '</div>') ?>
        <input type="text" name="login" value="<?= set_value('login') ?>" placeholder="login">
        <br />
        <label for="password">Mot de passe</label>
        <?= form_error('img-url', '<div class="error">', '</div>') ?>
        <input type="password" name="password" value="<?= set_value('password') ?>" placeholder="password">
        <br  /><br  /> -->
        <button href="#" class="button big fit">Ajouter</a>
    </form>
</div>
