

</div>

<!-- Scripts -->
<script src="<?= js_url('jquery.min.js') ?>"></script>
<script src="<?= js_url('skel.min.js') ?>"></script>
<script src="<?= js_url('util.js') ?>"></script>
<!--[if lte IE 8]><script src="<?= js_url('ie/respond.min.js') ?>"></script><![endif]-->
<script src="<?= js_url('template.js') ?>"></script>
<script src="<?= js_url('main.js') ?>"></script>

</body>
</html>
