<!DOCTYPE HTML>
<html>
<head>
    <title>BlogIT</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="<?= js_url('ie/html5shiv.js') ?>"></script><![endif]-->
    <!--[if lte IE 9]><link rel="stylesheet" href="<?= css_url('ie9.css') ?>" /><![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" href="<?= css_url('ie8.css') ?>" /><![endif]-->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->

    <link rel="stylesheet" href="<?= css_url('template.css') ?>" />
    <link rel="stylesheet" href="<?= css_url('main.css') ?>" />
</head>
<body>

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Header -->
        <header id="header">
            <h1><a href="<?= site_url('/') ?>">BlogIT</a></h1>
            <nav class="links">
                <ul>
                    <li><a href="<?= site_url('/blog/'.'?tag=developpement') ?>">Développement</a></li>
                    <li><a href="<?= site_url('/blog/'.'?tag=php') ?>">PHP</a></li>
                    <li><a href="<?= site_url('/blog/'.'?tag=javascript') ?>">JavaScript</a></li>
                    <li><a href="<?= site_url('/blog/'.'?tag=nosql') ?>">NoSQL</a></li>
                </ul>
            </nav>
            <nav class="main">
                <ul>
                    <?php if(!empty($connected_user)) : ?>
                        <li class="add-article">
                            <a class="fa-plus" href="<?= site_url('/article/add') ?>"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                        </li>
                    <?php endif; ?>
                    <li class="menu">
                        <a class="fa-bars" href="#menu">Menu</a>
                    </li>
                </ul>
            </nav>
        </header>

        <!-- Menu -->
        <section id="menu">

            <!-- Search -->
            <!-- <section>
                <form class="search" method="get" action="#">
                    <input type="text" name="query" placeholder="Search" />
                </form>
            </section> -->

            <!-- Links -->
            <section>
                <ul class="links">
                    <li>
                        <a href="<?= site_url('/blog/'.'?tag=developpement') ?>">
                            <h3>Développement</h3>
                            <p>Les dernières informatique sur le Développement</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('/blog/'.'?tag=php') ?>">
                            <h3>PHP</h3>
                            <p>Les dernières informatique sur le PHP</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('/blog/'.'?tag=javascript') ?>">
                            <h3>JavaScript</h3>
                            <p>Les dernières informatique sur le JavaScript</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('/blog/'.'?tag=nosql') ?>">
                            <h3>NoSQL</h3>
                            <p>Les dernières informatique sur le NoSQL</p>
                        </a>
                    </li>
                </ul>
            </section>

            <!-- Actions -->
            <section>
                <ul class="actions vertical">
                    <?php if($connected_user == FALSE) : ?>
                        <li><a href="<?= site_url('user/sign_in') ?>" class="button big fit">Connexion</a></li>
                        <li><a href="<?= site_url('user/sign_up') ?>" class="button big fit">Inscription</a></li>
                    <?php else : ?>
                        <li><a href="<?= site_url('user/sign_out') ?>" class="button big fit">Deconnexion</a></li>
                    <?php endif; ?>
                </ul>
                <!-- <ul class="actions">
                    <li><a href="#" class="button">Connexion</a></li>
                    <li><a href="#" class="button">Inscription</a></li>
                </ul> -->
            </section>

        </section>
