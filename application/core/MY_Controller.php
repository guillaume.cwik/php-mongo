<?php

/**
 * Extension du controller de base de CI
 */
class MY_Controller extends CI_Controller {

    protected $data = [];

    public function __construct() {
        parent::__construct();
        $this->connected_user = $this->session->userdata('user');
        $this->data['connected_user'] =& $this->connected_user;
    }

    protected function _load_header() {
        $this->load->view('common/header', $this->data);
    }

    protected function _load_footer() {
        $this->load->view('common/footer');
    }

    // Fonction utilisé pour la librairie form_validator
    public function is_unique(string $value, string $filter) {
		// TODO : faire pour pouvoir mettre plusieurs properties d'affilé
		$filter = explode('.', $filter);

		if(empty($filter) || count($filter) < 1) {
			throw new Exception("Le nom de la collection et les properties doivent être renseignées", 1);
		}

		$class_name = ucfirst(strtolower($filter[0]));
		$propertie = $filter[1];

		$object = $class_name::find_one([$propertie => $value]);
		return empty($object);
	}

}


 ?>
