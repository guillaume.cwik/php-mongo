<?php

/**
 * Surcharge la classe CI_Model pour y intégrer MongoDB
 */
class MY_Model extends CI_Model {

    public $mongo = null;
    public $db = null;
    public $collection = null;
    public $classe = 'Object';

    public function __construct() {
        parent::__construct();

        $this->config->load('mongodb', TRUE);
        $config = $this->config->item('mongodb');

        $this->mongo = new MongoDB\Client("mongodb://{$config['host']}:{$config['port']}");
        $this->db =& $this->mongo->{$config['database']};
    }

    /*
    Dans les fonctions ci-dessous, $filter peut être de type "array" ou "object"
    De même pour les variables $update dans les fonctions d'update
     */

    public function find_one($filter = [], array $options = []) {
        return $this->collection->findOne($filter, $options);
    }

    public function find($filter = [], array $options = []) {
        return $this->collection->find($filter, $options);
    }

    public function insert_one(Object $object, array $options = []) {
        return $this->collection->insertOne($object);
    }

    public function insert_many(array $objects, array $options = []) {
        return $this->collection->insertMany($objects);
    }

    public function replace_one($filter = [], $update, array $options = []) {
        return $this->collection->replaceOne($filter, $update, $options);
    }

    public function update_one($filter = [], $update, array $options = []) {
        return $this->collection->updateOne($filter, $update, $options);
    }

    public function update_many($filter = [], $update, array $options = []) {
        return $this->collection->updateMany($filter, $update, $options);
    }

    public function delete_one($filter = [], array $options = []) {
        return $this->collection->deleteOne($filter, $options);
    }

    public function delete_many($filter = [], array $options = []) {
        return $this->collection->deleteMany($filter, $options);
    }

    public function count($filter = [], array $options = []) {
        return $this->collection->count($filter, $options);
    }
}
